package pl.piotr.mpr7.chart;

public enum SerieType {
    Line,
    Point,
    LinePoint,
    Bar,
    Area
}
