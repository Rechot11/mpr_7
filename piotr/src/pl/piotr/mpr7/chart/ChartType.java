package pl.piotr.mpr7.chart;

public enum ChartType {
    Line,
    Point,
    LinePoint,
    Bar,
    StackedBar,
    Area,
    Pie
}
