package pl.piotr.mpr7.chart;

import org.junit.Test;

import static org.assertj.core.api.Assertions.*;

public class ChartBuilderTest {
    @Test
    public void testBuilder() {
        ChartSerie chartSerie = new SerieBuilder().addLabel("test").addPoint(new Point()).setType(SerieType.Point).build();

        ChartBuilder chartBuilder = new ChartBuilder();
        ChartSettings chartSettings = chartBuilder.addSerie(chartSerie)
                .withTitle("title")
                .withSubtitle("subtitle")
                .withType(ChartType.StackedBar)
                .build();

        assertThat(chartSettings.series.size()).isEqualTo(1);
        assertThat(chartSettings.series.get(0)).isEqualToComparingFieldByField(chartSerie);
        assertThat(chartSettings.title).isEqualToIgnoringCase("title");
        assertThat(chartSettings.subtitle).isEqualToIgnoringCase("subtitle");
        assertThat(chartSettings.chartType).isEqualTo(ChartType.StackedBar);
    }
}
